package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadentrypage extends ProjectMethods{
	
	
	public CreateLeadentrypage () {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement elecompName;
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement elefirstName;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	
	@FindBy(how = How.CLASS_NAME,using ="smallSubmit") WebElement eleCreateleadbut;



public CreateLeadentrypage enterCompanyName(String data) {
	
	clearAndType(elecompName,data);
	
	return this;
	
	
}

public CreateLeadentrypage enterfirstName(String data) {
	
	clearAndType(elefirstName,data);
	
	return this;
	
}

public CreateLeadentrypage enterLastName(String data) {

clearAndType(eleLastName,data);

return this;

}

public void clickCreateLeadbut() {
	
	click(eleCreateleadbut);
	 
}
}
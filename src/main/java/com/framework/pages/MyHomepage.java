package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyHomepage extends ProjectMethods {
	
	public MyHomepage() {
		
	PageFactory.initElements(driver, this);
	}
	//@FindBy(how = How.ID,using="username") WebElement eleUsername;
	//@FindBy(how = How.ID,using="password") WebElement elePassword;
	//@FindBy(how = How.CLASS_NAME,using="decorativeSubmit") WebElement el
	
	@FindBy(how = How.LINK_TEXT,using="Leads") WebElement eleLeads;
	
	public MyLeadspage clickLeads() {
		
		click(eleLeads);
		
		return new MyLeadspage();
		
	}
	
	
	
	
	
}

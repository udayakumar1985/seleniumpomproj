package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_Loginlogout extends ProjectMethods{

@BeforeTest

public void setdata() {
	
	testCaseName = "TC001_Loginlogout";
	testDescription ="Login to Leaftaps";
	testNodes="Leads";
	author = "Smoke";
	dataSheetName="TC001";
}

@Test(dataProvider="fetchData")

public void login(String username,String password) {
	 new LoginPage()
	.enterUsername(username)
	.enterPassword(password)
	.clickLogin()
	.clickLogout();
}

}

package com.framework.testcases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;
import com.framework.pages.MyHomepage;
import com.framework.pages.MyLeadspage;

public class TC002_CreateLead extends ProjectMethods{
	
	@BeforeTest
	
	
	public void setdata() {
	
	testCaseName = "TC002_CreateLead";
	testDescription ="Create Lead";
	testNodes="Lead";
	author = "Smoke";
	dataSheetName="TC002";
	
	
	
	}
	
	@Test(dataProvider="fetchData")
	
	public void login(String username,String password) {
		 new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin();
		

	    new HomePage()
	    .ClickCrmfa();
	    
	    new MyHomepage()
	    
	    .clickLeads();
	    
	    new MyLeadspage()
	    .ClickLeadpage();
	    
	    
	    
	    
		
		
		
}
}

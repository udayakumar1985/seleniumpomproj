Feature: CreateLead


Background: 

Given open chrome browser
And Enter the URL
And Enter the username as DemoCSR
And Enter the password as crmsfa
When click on login button
Then verify login is successful
And Click on CRMSFA
And Click on leads tab
And Click on create lead

@smoke
Scenario Outline: "Positive flow"

And Enter company name as <compname>
And Enter first name as <uname>
And Enter last name as <lname>
When click on Create Lead Button
Then verify lead is created successfully

Examples:
|compname|uname|lname|
|Newindia|Ragurma|U|
|Newindia|Udaya|S|

@sanity
Scenario Outline: "Negative flow"

And Enter first name as <uname>
And Enter last name as <lname>
When click on Create Lead Button
But Get the error message

Examples:
|uname|lname|
|jimmy|R|